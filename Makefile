.SILENT:

all: compiled
	javac thing/Main.java -d compiled/
	cp -r ./res/ ./compiled/

run:
	cd compiled/ && java thing.Main

clean: compiled
	rm -rf ./compiled/

compiled:
	mkdir ./compiled
	mkdir ./compiled/output/
