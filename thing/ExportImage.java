package thing;

import java.awt.image.BufferedImage;

public interface ExportImage{
	public BufferedImage getOutput(); //when used with SaveImage
}
