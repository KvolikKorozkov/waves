package thing;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;


public class CComponent extends Component implements Runnable, KeyListener, FocusListener{
	public Thread m_thread;
	public Scene scene;
	public Graphics2D g, gbi;
	public BufferedImage bi;
	public long fps;
	public boolean running;
	public CComponent(){
		m_thread = new Thread(this, "front thread");
		running = false;
		setFocusable(true);
		setEnabled(true);
		requestFocus();
		addKeyListener(this);
		addFocusListener(this);
	}
	public void start(){
		if(running) return;
		running = true;
		m_thread.start();
	}
	public void stop(){
		running = false;
	}
	public void run(){
		bi = (BufferedImage) getGraphicsConfiguration().createCompatibleImage(Main.size[0], Main.size[1]);
		gbi = bi.createGraphics();
		g = (Graphics2D) getGraphics();
		fps = getGraphicsConfiguration().getDevice().getDisplayMode().getRefreshRate();
		scene = new Scene7();
		long t_start, t_delta, t_wait;
		while(running){
			t_start = System.currentTimeMillis();
			scene.tick();
			scene.draw(gbi);
			g.drawImage(bi, 0, 0, null);
			t_delta = System.currentTimeMillis() - t_start;
			t_wait = fps - t_delta;
			if(t_wait < 0)continue;
			try{
				Thread.sleep(t_wait);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		System.exit(0);
	}
	public void focusGained(FocusEvent e){
	}
	public void focusLost(FocusEvent e){
	}
	public void keyPressed(KeyEvent e){
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
			stop();
		}
		switch(e.getKeyCode()){
			case KeyEvent.VK_1:
				scene = new Scene1(this);
				break;
			case KeyEvent.VK_2:
				scene = new Scene2(this);
				break;
			case KeyEvent.VK_3:
				scene = new Scene3(this);
				break;
			case KeyEvent.VK_4:
				scene = new Scene4(this);
				break;
			case KeyEvent.VK_5:
				scene = new Scene5(this);
				break;
		}
		e.consume();
	}
	public void keyReleased(KeyEvent e){
	}
	public void keyTyped(KeyEvent e){}
}
