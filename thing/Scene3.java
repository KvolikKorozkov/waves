package thing;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

public class Scene3 extends Scene{
	public CComponent comp;
	public Graphics2D gbi;
	public int imgw, imgh, himgw, himgh;
	public float ca, cb, cc, cd, td, cca, ccb, ccc, ccd;
	public float tau = (float) Math.PI*2;
	public Scene3(CComponent comp){
		this.comp = comp;
		//this.output = comp.bi;
		this.output = new BufferedImage(1901, 1900, BufferedImage.TYPE_INT_ARGB);
		this.gbi = (Graphics2D) output.getGraphics();
		this.imgw = output.getWidth();
		this.imgh = output.getHeight();
		this.himgw = imgw/2;
		this.himgh = imgh/2;
		this.td = comp.fps / 1000f;
	}
	public void tick(){
		float dx, dy, c;
		ca += td*.3f;
		if(ca > 1f) ca -= 2f;
		cb += td*.15f;
		if(cb > 1f) cb -= 2f;
		cc += td*.17f;
		if(cc > 1f) cc -= 2f;
		cd += td*.23f;
		if(cd > 1f) cd -= 2f;
		cca = (float) Math.sin(ca*tau);
		ccb = (float) Math.sin(cb*tau);
		ccc = (float) Math.cos(ca*tau);
		ccd = (float) Math.cos(cb*tau);
		for(int x=0; x<imgw;x++){
			for(int y=0;y<imgh;y++){
				dx = (float) Math.sin((himgw-x)/(120f));
				dy = (float) Math.cos((himgh-y)/(110f));
				c = dx*dx*ccc + dy*dy*ccd - (dy*ccb + dx*cca);
				output.setRGB(x, y, getColor(c));
			}
		}
	}
	int getColor(float c){
		int r, g, b;
		c = 2f * (float) Math.abs(c - Math.floor(c + .5));
		r = (int) (0x54 + c*(0x98-0x54));
		g = (int) (0x3a + (c*c * (0x84-0x3a)));
		b = (int) (0xea + (Math.sqrt(c) * (0xff-0xea)));
		return 0xff << 24 | r << 16 | g << 8 | b;
	}
	public void draw(Graphics2D g){}
}
