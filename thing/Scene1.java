package thing;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.Color;

public class Scene1 extends Scene{
	public int x, y, ca, rectw, recth;
	public float clock, speed, clr, tau = (float) (2*Math.PI), clockx, clocky;
	public BufferedImage bi;
	public Graphics2D big;
	public int[] colors = new int[]{
		//rr_gg_bb
		0x00_00_7f,
		0x00_3f_af,
		0x00_7f_df,
		0x00_ff_ff,
		0x00_af_ff,
		0x00_3f0_df,
	};
	public Scene1(CComponent c){
		ca = colors.length -1;
		speed = 0.02f;
		bi = c.bi;
		big = c.gbi;
		rectw = bi.getWidth();
		recth = bi.getHeight();
		clock = 1f;
		clocky = 0.3f;
		clockx = 0.2f;
		big.clearRect(0, 0, Main.size[0], Main.size[1]);
	}
	public void tick(){
		clock += speed*.26;
		if(clock > 1f) clock -= 2f;
		clockx += speed*0.33;
		if(clockx > 1f) clockx -= 2f;
		clocky += speed*0.22;
		if(clocky > 1f) clocky -= 2f;
		float sx = (float) (Math.sin(clockx*tau));
		float sy = (float) (Math.cos(clockx*tau));
		for(x=0; x < rectw; x++){
			for(y=0; y < recth; y++){
				float _x = (float) Math.sin(((float)x)/(Main.size[0]>>4));
				_x += (float) Math.cos((float)(x))/200;
				float _y = (float) Math.cos(((float)y)/(Main.size[1]>>4));
				_y += (float) Math.sin((float)(y))/500;
				clr = (_x*_x*((sy)*tau)) +(_y*_y*((sx)*tau));
				clr += _x*sy*sy + _y*sy*sy*tau + Math.cos(x*y*tau);
				clr /= 4;
				bi.setRGB(x, y, getColor(clr));
			}
		}
	}
	public int getColor(float i){
		int r, g, b;
		i = 2f * (float) Math.abs(i - (float) Math.floor(i + .5f));
		g = (int) (i * 0xff) & 0xff;
		b = (int) (Math.sqrt(i) * 0xaf) & 0xff;
		return g << 8 | b;
	}
	public void draw(Graphics2D g){
	}
}
