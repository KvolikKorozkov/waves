package thing;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;
import java.awt.image.BufferedImage;

public class SaveImage{
	public static Scene scene;
	public static void renderImage(){
		File file;
		for(int iter=0; iter < 51; iter++){
			scene.tick();
			try{
				file = new File("./output/" + padRight(Integer.toString(iter), 3, '0') + ".png");
				ImageIO.write(scene.output, "png", file);
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
	//if length is less than base.length, it shits itself
	//leave it like that cuz I have use cases where this is desirable
	public static String padRight(String base, int length, char fill){
		int len_base = base.length();
		char[] nstr = new char[length];
		for(int i=0; i<length-len_base; i++){
			nstr[i] = fill;
		}
		for(int i=0; i<len_base; i++){
			nstr[length-len_base + i] = base.charAt(i);
		}
		return new String(nstr);
	}
}
