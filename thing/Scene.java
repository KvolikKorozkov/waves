package thing;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public abstract class Scene{
	BufferedImage output;
	public abstract void tick();
	public abstract void draw(Graphics2D g);
}
