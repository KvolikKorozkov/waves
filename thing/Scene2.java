package thing;

import thing.*;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Scene2 extends Scene{
	public CComponent comp;
	public BufferedImage img;
	public int imgw, imgh;
	public float tau = (float) Math.PI * 2, dt, clock, clocka, clockb, _ca, _cb, _cc;
	public Scene2(CComponent comp){
		comp = comp;
		img = comp.bi;
		imgw = img.getWidth();
		imgh = img.getHeight();
		dt = 0.005f;
		clocka = 0.5f;
		clockb = -0.7f;
	}
	public void tick(){
		int color = 0;
		float clr=0, _x, _y;
		clock += dt;
		if(clock > 1f) clock -= 2f;
		clocka += dt* .5f;
		if(clocka > 1f) clocka -= 2f;
		clockb += dt* .7f;
		if(clockb > 1f) clockb -= 2f;
		_ca = (float) Math.cos(clocka*tau);
		_cb = (float) Math.sin(clocka*tau);
		_cc = (float) Math.cos(clock*tau);
		for(int x=0; x < imgw; x++){
			for(int y=0; y < imgh; y++){
				_x = (float) Math.cos((float)(x)/20);
				_x += (float) Math.cos((float)x/200);
				_y = (float) Math.sin((float)y/20);
				_y += (float) Math.sin((float)y/200);
				clr = (float) (_x*_x*_ca + _y*_y*_cb + _x*_y*_cc);
				img.setRGB(x, y, getColor(clr));
			}
		}
	}
	int getColor(float i){
		int r, g, b;
		i = 2f * (float) Math.abs(i - (float) Math.floor(i + .5f));
		r = (int) (Math.sqrt(i) * 0x4f) & 0xff;
		g = (int) (i * 0x2f) & 0xff;
		b = (int) ((i*i) * 0x7f) & 0xff;
		return r << 16 | g << 8 | b;
	}
	public void draw(Graphics2D g){}
}
