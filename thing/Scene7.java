package thing;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

public class Scene7 extends Scene{
	BufferedImage fore, eyes, back, t_fore, t_back, alt_fore, alt_back;
	Graphics2D gout;
	int[] eye_pos = {-2, 2};
	int counter=0;
	public Scene7(){
		fore = Image.loadFrom("./res/001_triipy_fore.png");
		eyes = Image.loadFrom("./res/001_triipy_eyes.png");
		back = Image.loadFrom("./res/001_triipy_back.png");
		alt_fore = Image.loadFrom("./res/001_triipy_fore_alt.png");
		alt_back = Image.loadFrom("./res/001_triipy_back_alt.png");
		output = new BufferedImage(fore.getWidth(), fore.getHeight(), BufferedImage.TYPE_INT_ARGB);
		t_fore = new BufferedImage(fore.getWidth(), fore.getHeight(), BufferedImage.TYPE_INT_ARGB);
		t_back = new BufferedImage(back.getWidth(), back.getHeight(), BufferedImage.TYPE_INT_ARGB);
		gout = (Graphics2D) output.getGraphics();
	}
	public void tick(){
		int x, y;
		double a, countw, countx;
		counter++;
		//when tick counter is greater than ticks per cycle
		if(counter > 50){
			counter -= 50;
		}
		//counter / (ticks per cycle/2)
		//pi is for normalizing
		countw = Math.sin((counter/25d)*Math.PI);
		countx = Math.cos((counter/25d)*Math.PI);
		for(x=0; x < output.getWidth(); x++){
			for(y=0; y < output.getHeight(); y++){
				//background
				a = Math.sqrt(Math.pow((countw*25) + 275 - x, 2) + Math.pow(250 - y, 2))/150;
				a *= Math.sin((countw*250+a+x)/250) * Math.cos((countx*250+a+y)/250);
				output.setRGB(x, y, getColor(0xff00ffff, 0xff7f00ff, a));
				//tint char fore and background colors
				a = Math.sin(((countx*150 + x)*Math.PI) / 150d);
				a += Math.cos(((countw*150 + y)*Math.PI) / 150d);
				a *= Math.cos((Math.sqrt(x*x) + Math.sqrt(y*y)) /200d);
				t_back.setRGB(x, y, getColor(back.getRGB(x, y), alt_back.getRGB(x, y), a));
				t_fore.setRGB(x, y, getColor(fore.getRGB(x, y), alt_fore.getRGB(x, y), a));
			}
		}
		//paint to output
		gout.drawImage(t_back, 0, 0, null);
		gout.drawImage(eyes, eye_pos[counter %2], 0, null);
		gout.drawImage(t_fore, 0, 0, null);
	}
	public int getColor(int start, int end, double step){
		int r, g, b, sr, sg, sb, er, eg, eb;
		double j = 2 * Math.abs(step - Math.floor(step+.5));
		sr = (start >> 16) & 0xff;
		er = (end >> 16) & 0xff;
		sg = (start >> 8) & 0xff;
		eg = (end >> 8) & 0xff;
		sb = start & 0xff;
		eb = end & 0xff;
		r = sr + (int) (j*(er-sr));
		g = sg + (int) (j*j*(eg-sg));
		b = sb + (int) (Math.sqrt(j)*(eb-sb));
		return (start&0xff000000) | r << 16 | g << 8 | b;
	}
	public void draw(Graphics2D g){
		g.drawImage(output, 0, 0, null);
	}
}
