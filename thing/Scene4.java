package thing;

import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Scene4 extends Scene{
	public CComponent comp;
	public BufferedImage pic, bi, sprite;
	public double tau = Math.PI*2;
	public float mc, c1, c2, c3;
	public int t;
	public int[] fur = {
			0xff_fe_fe, 0xff_f7_f7, 0xff_f0_f0, 0xff_df_df,
			0xff_d0_d0, 0xff_df_df, 0xff_f0_f0, 0xff_f7_f7},
		hair = {
			0xaf_94_6e, 0xa9_85_66, 0xa7_7a_5c, 0xa3_6f_51,
			0xa0_69_49, 0xa3_6f_51, 0xa7_7a_5c, 0xa9_85_66},
		eye = {
			0x82_53_36, 0x7f_41_24, 0x79_33_1a, 0x6c_22_12,
			0x5e_14_09, 0x6c_22_12, 0x79_33_1a, 0x7f_41_24};
	public Graphics2D big;
	public int imgw, imgh;
	public Scene4(CComponent comp){
		this.comp = comp;
		this.bi = comp.bi;
		this.big = comp.gbi;
		comp.gbi.clearRect(0, 0, Main.size[0], Main.size[1]);
		init();
	}
	public void init(){
		try{
			pic = ImageIO.read(new File("./kv_map.png"));
		}catch(IOException e){
			e.printStackTrace();
			comp.stop();
			return;
		}
		imgw = pic.getWidth();
		imgh = pic.getHeight();
		sprite = new BufferedImage(imgw, imgh, BufferedImage.TYPE_INT_ARGB);
	}
	public void tick(){
		int color;
		double _x, _y;
		t++;
		if(t > 255) t -= 255;
		mc = t/255f;
		c1 = (float) Math.sin(mc * tau);
		c2 = (float) Math.cos(mc * tau);
		c3 = (float) Math.sin(3*mc*tau);
		for(int x=0; x<imgw; x++){
			for(int y=0; y<imgh; y++){
				color = pic.getRGB(x, y);
				if(color >> 28 == 0){
					continue;
				}
				switch(color){
					case 0xff_cc_00_00: //fur
						_x = (float) x/15 - (float) y/37;
						_y = (float) y/15 - (float) x/37;
						color = fur[(int) (_x*_x*c1 - _y*_y*c2) & 0x7];
						break;
					case 0xff_4e_9a_06: //hair
						_x = ((float)x/20) *c1;
						_y = ((float)y/20) *c2;
						color = hair[(int) (_x*_x + _y*_y + 2*_x*_y) & 0x7];
						break;
					case 0xff_c4_a0_00: // eye
						color = 0xdf_df_df;
						break;
					case 0xff_34_65_a4: // iris
						_x = (((float)x - 150f) /4) * c2;
						_y = (((float)y - 80f) /4) * c1;
						color = eye[(int) (_x*_x*c2 + _y*_y*c1 + 18f) & 0x7];
						//color = eye[0];
						break;
				}
				sprite.setRGB(x, y, 0xff << 28 | color);
			}
		}
	}
	public void draw(Graphics2D g){
		g.drawImage(sprite, 0, 0, Main.size[0], Main.size[1], null);
	}
}
