package thing;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Image{
	public static BufferedImage loadFrom(String path){
		File file;
		BufferedImage img;
		try{
			file = new File(path);
			img = ImageIO.read(file);
		}catch(IOException e){
			e.printStackTrace();
			return null;
		}
		return img;
	}
}

