package thing;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Scene5 extends Scene{
	public CComponent c;
	public BufferedImage map, glitch, result;
	public Graphics2D glitchg;
	public int width, height, offset, clra;
	public float wave_clock;
	public int[] colors = {
		0x00000000, //padding
		0x00000000,
		0xffff0000,
		0xff00e9ff,
		0xffff7f00,
		0xff00e9ff,
		0xffff0000,
		0xff00e9ff,
		0xffffff00,
		0xff00e9ff,
		0xff3fff3f,
		0xff00e9ff,
		0xff7f00ff,
		0xff00e9ff,
		0xffbf00ff,
		0xff00e9ff,
		0xffffff00,
		0xff00e9ff,
		0xffff0000,
		0xff00e9ff,
		0x00000000,
		0x00000000
	};
	public Scene5(CComponent c){
		this.c = c;
		try{
			File img = new File("./res/image.png");
			map = ImageIO.read(img);
		}catch(IOException e){
			e.printStackTrace();
			c.stop();
			return;
		}
		offset = 0;
		clra = colors.length - 1;
		width = map.getWidth();
		height = map.getHeight();
		glitch = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		glitchg = (Graphics2D) glitch.getGraphics();
		glitchg.setBackground(new java.awt.Color(0x3f, 0x3f, 0x3f));
		result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		c.gbi.clearRect(0, 0, Main.size[0], Main.size[1]);
	}
	public void tick(){
		wave_clock += 0.03f;
		if(wave_clock >= 1f){
			wave_clock -= 2f;
		}
		//generate random glitch map
		glitchg.clearRect(0, 0, width, height);
		for(int i=0; i<16; i++){ //16 could be any number here
			glitchg.setColor(new java.awt.Color(colors[(int) (Math.random()*clra)])); //this could be done better
			glitchg.fillRect(
				(int) (Math.random()*width), (int) (Math.random()*height),
				(int) (1+Math.random()*19), (int) (1+Math.random()*15)
			);
		}
		int mclr;
		for(int x=0; x<width; x++){
			for(int y=0; y<height; y++){
				mclr = map.getRGB(x, y);
				switch(mclr){
					case 0xff0000c0:
						float a = y - offset;
						a = (a*clra) / height;
						a += (float) Math.sin(wave_clock*Math.PI + x*0.3f)*0.2f;
						if(a < 0) a = 0;
						result.setRGB(x, y, colors[(int) a]);
						break;
					case 0xff0000ff:
						result.setRGB(x, y, glitch.getRGB(x, y));
						break;
					case 0xffffffff:
						result.setRGB(x, y, 0xff5fff2f);
						break;
					case 0xffc0c0c0:
						result.setRGB(x, y, 0xffc0c0c0);
					default:
						result.setRGB(x, y, mclr);
				}
			}
		}
	}
	public void draw(Graphics2D g){
		g.drawImage(result, 0, 0, Main.size[0], Main.size[1], null);
	}
} 
