package thing;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

public class Scene6 extends Scene{
	public float a, b, c;
	public int ow, oh;
	public Scene6(CComponent comp){
		ow = 200;
		oh = 100;
		output = new BufferedImage(ow, oh, BufferedImage.TYPE_INT_ARGB);
	}
	public void tick(){
		for(int x=0; x < ow; ++x){
			for(int y=0; y < oh; ++y){
				float _x = (float) (Math.sin(x/120f) * Math.cos(y/120f));
				output.setRGB(x, y, getColor(_x));
			}
		}
	}
	public int getColor(float i){
		float q = 2* (float) Math.abs(i - (i+0.5));
		int r, g, b;
		r = (int) Math.floor(q+0.02f);
		g = (int) (0x0f + (q*q)*(0xff - 0x0f));
		b = (int) Math.sqrt(q) * 0xff;
		return 0xff000000 | r << 16 | g << 8 | b;
	}
	public void draw(Graphics2D g){}
}
