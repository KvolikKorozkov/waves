package thing;

import java.awt.Frame;

public class Main extends Frame{
	public static Main m;
	public static int[]	size = {600, 480};
	public static CComponent c;
	public Main(){
		super();
		setTitle("waves thing");
		setSize(size[0], size[1]);
		setResizable(false);
		setLocationRelativeTo(null);
		setBackground(new java.awt.Color(12, 0, 17));
		c = new CComponent();
		add(c, 0);
		setVisible(true);
	}
	public static void main(String[] args){
		javax.swing.SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				//m = new Main();
				//c.start();
				SaveImage.scene = new Scene7();
				SaveImage.renderImage();
			}	
		});
	}
}
